export type CreateArticleDto = {
    titre: string;
    description: string;
    image: string;
    nom: string;
    prenom: string;
    ville: string;
    email: number;
};