import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import {ArticleController} from "./controllers/article.controller";
import {ArticleService} from "./services/article.service";

@Module({
  imports: [
    TypeOrmModule.forRoot({
        type: 'mysql',
        host: 'localhost',
        port: 3306,
        username: 'root',
        password: 'root',
        database: 'yamay',
        entities: ['src/entities/**.entity.ts'],
        synchronize: true,
    }),
  ],
  controllers: [
      AppController,
      ArticleController
  ],
  providers: [
      AppService,
      ArticleService
  ],
})
export class AppModule {}
