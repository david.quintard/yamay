import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import {Column, Repository} from 'typeorm';
import { Article } from '../entities/article.entity';
import {CreateArticleDto} from "../dtos/create-article.dto";

@Injectable()
export class ArticleService {
    constructor(
        @InjectRepository(Article)
        private articleRepository: Repository<Article>,
    ) {}

    findAll(): Promise<Article[]> {
        return this.articleRepository.find();
    }

    insert(body: CreateArticleDto): Promise<Article> {
        const today = new Date().toISOString().slice(0, 10)

        const article: Article = new Article();
        article.title = body.titre;
        article.description = body.description;
        // article.image = body.image;
        article.name = body.nom;
        article.firstname = body.prenom;
        article.city = body.ville;
        // article.email = body.email;
        article.status = 1;
        article.updated_at = today;
        article.created_at = today;

        return this.articleRepository.save(article);
    }

    findOne(id: string): Promise<Article> {
        return this.articleRepository.findOne(id);
    }

    async remove(id: string): Promise<void> {
        await this.articleRepository.delete(id);
    }


}