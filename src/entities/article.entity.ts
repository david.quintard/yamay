import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Article {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    title: string;

    @Column()
    description: string;

    @Column()
    name: string;

    @Column()
    firstname: string;

    @Column()
    city: string;

    @Column()
    status: number;

    @Column()
    updated_at: string;

    @Column()
    created_at: string;
}