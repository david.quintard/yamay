import {Body, Controller, Get, Post, UploadedFile, UseInterceptors} from '@nestjs/common';
import { ArticleService } from '../services/article.service';
import {Article} from "../entities/article.entity";
import {CreateArticleDto} from "../dtos/create-article.dto";
import {FileInterceptor} from "@nestjs/platform-express";

@Controller('articles')
export class ArticleController {
  constructor(private readonly articleService: ArticleService) {}

  @Get()
  findAll(): Promise<Article[]> {
    return this.articleService.findAll();
  }

  @Post()
  @UseInterceptors(FileInterceptor('file'))
  create(
      @UploadedFile('file') file: Express.Multer.File,
      @Body() body: CreateArticleDto): Promise<Article> {
    const binary = file.buffer;
    return this.articleService.insert(body);
  }
}
